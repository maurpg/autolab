""" Module to define api pokemon endpoints tests """
import json
from django.test import TestCase
from AutoLabApp.models import Pokemon
import pytest


class TestPokemon(TestCase):
    """ Test case for Pokemon endpoint /api/pokemon/action """

    def test_endpoint_should_avoid_create_pokemon(self):
        payload = {
            "name": 'pikachu',
            "type_1": "test_1",
            "type_2": "test_2",
        }
        response = self.client.post('/api/pokemon/', data=payload, content_type='application/json',)
        self.assertEqual(response.status_code, 201)

    def test_endpoint_should_validate_mandatory_data(self):

        """ Endpoint /api/pokemon/ must validate the next data,
        {
            'name': 'string and not mandatory',
            'type_1': 'string and not mandatory',
            'type_2': 'string and not mandatory',
            'total': 'integer and not mandatory',
            'hp': integer and not mandatory
        }
        if the data is malformed or missing a 400 response is expected.
        """

        bad_values = [
            {},
            {'name': 'mimosa'},
            {'name': 'test', 'type_1': 45, 'type_2': 454, 'total': 'test'},
            {'name': 233, 'type_1': 4546, 'type_2': 2021, 'hp': 'test'},
        ]
        for payload in bad_values:
            response = self.client.post('/api/pokemon/', data=payload, content_type='application/json', )
            self.assertEqual(response.status_code, 400)

    def test_endpoint_with_filter(self):
        """
        test list with query params, endpoint should be response with 200 status code
        :return:
        """
        params = {'name': 'pikachu', 'type_1': 'test_1'}
        response = self.client.get('/api/pokemon/', params=params, content_type='application/json',)
        self.assertEqual(response.status_code, 200)



