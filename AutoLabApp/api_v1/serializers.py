from rest_framework import serializers

from AutoLabApp.models import Pokemon


class PokemonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pokemon
        fields = ('__all__')