from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from AutoLabApp.api_v1.serializers import PokemonSerializer
from AutoLabApp.models import Pokemon
from django_filters.rest_framework import DjangoFilterBackend


class PokemonService(viewsets.ModelViewSet):
    """
    PokemonServices is a ViewSet for all service of pokemon class
    allowed method are create, edit, list, retrieve and delete the lasts
    needed id of object for action
    """
    queryset = Pokemon.objects.all()
    serializer_class = PokemonSerializer
    filter_backends = [DjangoFilterBackend]
    permission_classes = (IsAuthenticated, )
    # some filters
    filterset_fields = ['name', 'type_1', 'type_2', 'generation', 'legendary']

