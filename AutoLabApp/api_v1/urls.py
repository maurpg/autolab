from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from AutoLabApp.api_v1.viewset import PokemonService
from django.urls import path
from rest_framework_simplejwt import views as jwt_views

router = routers.SimpleRouter()
router.register(r'pokemon', PokemonService)
urlpatterns = router.urls


urlpatterns += [
    # Your URLs...
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]
