from django.db import models


class Pokemon(models.Model):
    name = models.CharField(max_length=100, null=True)
    type_1 = models.CharField(max_length=100, null=True)
    type_2 = models.CharField(max_length=100, null=True)
    total = models.IntegerField(null=True)
    hp = models.IntegerField(null=True)
    attack = models.IntegerField(null=True)
    defense = models.IntegerField(null=True)
    sp_attack = models.IntegerField(null=True)
    sp_defense = models.IntegerField(null=True)
    generation = models.IntegerField(null=True)
    legendary = models.BooleanField(null=True)
