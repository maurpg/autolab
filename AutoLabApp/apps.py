from django.apps import AppConfig


class AutolabappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'AutoLabApp'
