# AutoLab 

This project exposes web services for management Pokemon services

## Main dependencies
* Python 3.6
* Django 3.2.6
* djangorestframework 3.12

## Features

***
* Get access token Service http://127.0.0.1:8000/api/token
* Pokemon Service http://127.0.0.1:8000/api/pokemon/{action}

##
* Json Example for create Pokemon
```json
        {
            "name": "pikachu",
            "type_1": "Electrict",
            "type_2": "Fire",
            "total": 30,
            "attack": 566,
            "hp": 60000
        }
```
 
## Installation

Docker

* You machine should has docker and docker-compose installed

```bash
$ docker-composer -f local.yml up
```

## Note

* Remember that to activate the services it is necessary to be authenticated, to authenticate and obtain the token it is necessary to point to the following url "http://127.0.0.1:8000/api/token"  send the username and password